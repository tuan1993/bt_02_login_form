<?php
session_start();
require_once("functions/functions.php");

$username = getValue("username", "POST", "str", "");
$password = getValue("password", "POST", "str", "");
$action = getValue("action", "POST", "str", "");


// var_dump($_POST);
// var_dump($username);
// var_dump($password);

$errorMsg = "";
if ($action == "login") {
    if ($username == "") {
        $errorMsg .= " Vui lòng nhập User Name.<br />";
    }
    if ($password == "") {
        $errorMsg .= "Vui lòng nhập Password.<br />";
    }

    // Nếu có đủ dữ liệu POST thì xác thực
    if ($errorMsg == "") {
        if ($username == "admin" && $password == "123456") {
            // Success
            // echo "Success";
            $_SESSION["logged"] = 1;
            header("Location: themmoi.php");
        } else {
            $errorMsg .= "Thông tin đăng nhập không đúng. Vui lòng thử lại.<br />";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            background-color: #fff;
          
            /* background-image: ; */
        }

        #login .container #login-row #login-column #login-box {
            margin-top: 30px;
            max-width: 600px;
            height: 320px;
            border: 1px solid #9C9C9C;
            background-color: #EAEAEA;
        }

        #login .container #login-row #login-column #login-box #login-form {
            padding: 20px;
        }

        #login .container #login-row #login-column #login-box #login-form #register-link {
            margin-top: -85px;
        }

        #login-err-msg {
            width: 540px;
            margin: 30px auto;
        }
        .left-side{
            background-image: url('https://img.freepik.com/free-vector/japanese-wave-line-art-landscape-background-abstract-mountain-banner-design-pattern-vector-illustration-geometric-poster_90220-714.jpg?w=1380&t=st=1661703362~exp=1661703962~hmac=1e7bd409927eb2d1942319884904aa4a6f05092cb68cdf46f195df3cb485ec91');
            background-position: center center;
            background-size: cover;
            background-repeat: no-repeat;
        }
    </style>
    <title>Login</title>
</head>

<body >
    <div id="login">
        <div class="">

            <div class="row no-gutters" style="  height: 100vh;">
                <div class="col-md-6 left-side" ></div>
                <div class="col-md-6">
                <div id="login-row" class="row no-gutters justify-content-center align-items-center" style="height:100%">
           
           <div id="login-column" class="col-md-11">
           <a  href="https://dongtuan.myshopify.com" title="Đồng Đăng Tuấn" target="_blank"><img  src="https://d2xrtfsb9f45pw.cloudfront.net/logos/1657421916_OnCEJ" alt="Logo" style="width: 70px;"></a>
               <div id="login-box" class="col-md-11 border py-4 px-8 mt-4" style="border-radius: 6px;">
                   <form id="login-form" class="form" action="" method="post">
                       <h3 class=" ">Login</h3>
                       <div class="form-group">
                           <label for="username" class="">Username</label><br>
                           <input type="text" name="username" id="username" class="form-control" value="<?= $username ?>">
                       </div>
                       <div class="form-group">
                           <label for="password" class="">Password</label><br>
                           <input type="password" name="password" id="password" class="form-control" value="<?= $password ?>">
                       </div>
                       <div class="form-group">
                           <!-- <label for="remember-me" class=""><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br> -->
                           <input type="hidden" id="action" name="action" value="login" />
                           <input type="submit" name="submit" class="btn btn-primary" value="submit">
                       </div>
                       <?php if ($errorMsg != "") { ?>
                <div class="alert alert-danger" id="login-err-msg">
                    <?= $errorMsg ?>
                </div>
            <?php } ?>
                       <!-- <div id="register-link" class="text-right">
                           <a href="#" class="">Register here</a>
                       </div> -->
                   </form>
               </div>
           </div>
       </div>
                </div>
            </div>

        </div>
    </div>
</body>

</html>

<!------ Include the above in your HEAD tag ---------->